const { VueLoaderPlugin } = require('vue-loader')
const path = require('path');
module.exports = {
  entry: [
    './vue/app.js'
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  output: {
      filename: 'app.js',
      path:  path.resolve(__dirname, 'public')
  }
}